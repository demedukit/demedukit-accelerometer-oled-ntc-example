################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lis2dw12/lis2dw12_reg.c 

OBJS += \
./lis2dw12/lis2dw12_reg.o 

C_DEPS += \
./lis2dw12/lis2dw12_reg.d 


# Each subdirectory must supply rules for building sources it contributes
lis2dw12/%.o: ../lis2dw12/%.c lis2dw12/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32G030xx -c -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -I../lis2dw12 -I../DemsayLib/Inc -I"C:/Users/Onur/Desktop/DemsayRD/Software/DEMEDUKIT/demedukit-accelerometer-oled-ntc-example/ssd1306" -O0 -ffunction-sections -fdata-sections -Wall -fcommon -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-lis2dw12

clean-lis2dw12:
	-$(RM) ./lis2dw12/lis2dw12_reg.d ./lis2dw12/lis2dw12_reg.o

.PHONY: clean-lis2dw12

