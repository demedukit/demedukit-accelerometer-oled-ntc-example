################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../DemsayLib/Src/DigitalInputOutputs.c 

OBJS += \
./DemsayLib/Src/DigitalInputOutputs.o 

C_DEPS += \
./DemsayLib/Src/DigitalInputOutputs.d 


# Each subdirectory must supply rules for building sources it contributes
DemsayLib/Src/%.o: ../DemsayLib/Src/%.c DemsayLib/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32G030xx -c -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -I../lis2dw12 -I../DemsayLib/Inc -I"C:/Users/Onur/Desktop/DemsayRD/Software/DEMEDUKIT/demedukit-accelerometer-oled-ntc-example/ssd1306" -O0 -ffunction-sections -fdata-sections -Wall -fcommon -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-DemsayLib-2f-Src

clean-DemsayLib-2f-Src:
	-$(RM) ./DemsayLib/Src/DigitalInputOutputs.d ./DemsayLib/Src/DigitalInputOutputs.o

.PHONY: clean-DemsayLib-2f-Src

